// rcr(rcredux)

import React, { Component } from "react";
import { connect } from "react-redux";
import { CHOOSE, TAI, XIU } from "./redux/constant/XucXacConstant";

export class XucXac extends Component {
    state = {
        active: null,
    };
    handleChangeActive = (value) => {
        this.setState({ active: value });
    };

    renderListXucXac = () => {
        return this.props.mangXucXac.map((item, index) => {
            return (
                <img
                    src={item.img}
                    alt=""
                    key={index}
                    style={{ width: 100, margin: 10 }}
                />
            );
        });
    };
    render() {
        return (
            <>
                <h1 className="pt-5">Game Xúc Xắc</h1>
                <div className="d-flex justify-content-between container pt-5">
                    <button
                        className="btn btn-danger "
                        onClick={() => {
                            this.handleChangeActive(TAI);
                            this.props.handleChoose(TAI);
                        }}
                        style={{
                            width: 150,
                            height: 150,
                            fontSize: 40,
                            transform: `scale(${
                                this.state.active === TAI ? 2 : 1
                            })`,
                            transition: "0.5s",
                        }}
                    >
                        Tài
                    </button>
                    <div>{this.renderListXucXac()}</div>
                    <button
                        className="btn btn-dark "
                        onClick={() => {
                            this.handleChangeActive(XIU);
                            this.props.handleChoose(XIU);
                        }}
                        style={{
                            width: 150,
                            height: 150,
                            fontSize: 40,
                            transform: `scale(${
                                this.state.active === XIU ? 2 : 1
                            })`,
                            transition: "0.5s",
                        }}
                    >
                        Xỉu
                    </button>
                </div>
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    mangXucXac: state.xucXacReducer.mangXucXac,
});

const mapDispatchToProps = (dispatch) => {
    return {
        handleChoose: (value) => {
            dispatch({
                type: CHOOSE,
                payload: value,
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(XucXac);
