import { CHOOSE, PLAY_GAME, TAI, XIU } from "./constant/XucXacConstant";

let initialState = {
    mangXucXac: [
        {
            img: "./imgXucSac/1.png",
            giaTri: 1,
        },
        {
            img: "./imgXucSac/1.png",
            giaTri: 1,
        },
        {
            img: "./imgXucSac/1.png",
            giaTri: 1,
        },
    ],
    luaChon: null,
    tongSoLanChoi: 0,
    tongSoLanThang: 0,
    ketQua: null,
};

export const xucXacReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case PLAY_GAME: {
            // tao mang xuc xac moi
            let newMangXucXac = state.mangXucXac.map((item) => {
                let random = Math.floor(Math.random() * 6) + 1;
                return {
                    img: `./imgXucSac/${random}.png`,
                    giaTri: random,
                };
            });
            let newSoLanChoi = state.tongSoLanChoi;
            newSoLanChoi++;

            let tongDiem = 0;
            let newTongSoLanThang = state.tongSoLanThang;
            state.mangXucXac.forEach((item) => {
                tongDiem += item.giaTri;
            });
            if (tongDiem >= 11) {
                if (state.luaChon === TAI) {
                    newTongSoLanThang++;
                    state.ketQua = "YOU WIN";
                } else {
                    state.ketQua = "YOU LOSE";
                }
            } else {
                if (state.luaChon === XIU) {
                    newTongSoLanThang++;
                    state.ketQua = "YOU WIN";
                } else {
                    state.ketQua = "YOU LOSE";
                }
            }

            return {
                ...state,
                mangXucXac: newMangXucXac,
                tongSoLanChoi: newSoLanChoi,
                tongSoLanThang: newTongSoLanThang,
            };
        }
        case CHOOSE: {
            let newLuaChon = state.luaChon;
            newLuaChon = payload;
            return { ...state, luaChon: newLuaChon };
        }
        default:
            return state;
    }
};

// >=11 : tai
