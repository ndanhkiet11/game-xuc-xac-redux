import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME } from "./redux/constant/XucXacConstant";

class KetQua extends Component {
    render() {
        return (
            <div className="text-center pt-5 display-4">
                <button
                    onClick={this.props.handlePlayGame}
                    className="btn btn-success "
                >
                    <span className="display-4">Play Game</span>
                </button>
                <h3 className="text-danger">{this.props.ketQua}</h3>
                <div className="mt-5 text-light">
                    <h3>BẠN CHỌN: {this.props.luaChon}</h3>
                    <h4>Tổng số lần Bạn Thắng: {this.props.soLanThang}</h4>
                    <h4>Tổng số lần Tung Xúc Xắc: {this.props.soLanChoi}</h4>
                </div>
            </div>
        );
    }
}

let mapStateToProps = (state) => ({
    soLanChoi: state.xucXacReducer.tongSoLanChoi,
    soLanThang: state.xucXacReducer.tongSoLanThang,
    luaChon: state.xucXacReducer.luaChon,
    ketQua: state.xucXacReducer.ketQua,
});

let mapDispatchToProps = (dispatch) => {
    return {
        handlePlayGame: () => {
            dispatch({
                type: PLAY_GAME,
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
